package com.dh.chat.contact.service.controller.api;

import com.dh.chat.contact.api.input.ContactSearchInput;
import com.dh.chat.contact.service.command.ContactSearchCmd;
import com.dh.chat.contact.service.commons.Pagination;
import com.dh.chat.contact.service.controller.Constants;
import com.dh.chat.contact.service.model.domain.Contact;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = Constants.ContactTag.NAME,
        description = Constants.ContactTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SECURE_CONTACTS)
@RequestScope
public class ContactSearchController {

    @Autowired
    private ContactSearchCmd contactSearchCmd;

    @ApiOperation(
            value = "Search contacts"
    )
    @RequestMapping(
            value = "/search",
            method = RequestMethod.POST)
    public Pagination<Contact> searchContact(@RequestHeader("Account-ID") Long accountId,
                                             @RequestHeader("User-ID") Long userId,
                                             @RequestParam("limit") Integer limit,
                                             @RequestParam("page") Integer page,
                                             @RequestBody ContactSearchInput input) {
        contactSearchCmd.setAccountId(accountId);
        contactSearchCmd.setUserId(userId);
        contactSearchCmd.setPage(page);
        contactSearchCmd.setLimit(limit);
        contactSearchCmd.setInput(input);
        contactSearchCmd.execute();

        Pagination<Contact> pagination = new Pagination<>();
        pagination.setContent(contactSearchCmd.getContacts());
        pagination.setTotalPages(contactSearchCmd.getTotalPages());
        pagination.setTotalElements(contactSearchCmd.getTotalElements());
        System.out.println("After pagination build");
        return pagination;
    }
}
