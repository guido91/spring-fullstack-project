package com.dh.chat.contact.service.command;

import com.dh.chat.contact.api.input.ContactSearchInput;
import com.dh.chat.contact.service.model.domain.Contact;
import com.dh.chat.contact.service.model.domain.Contact_;
import com.dh.chat.contact.service.model.domain.Detail;
import com.dh.chat.contact.service.model.domain.Detail_;
import com.dh.chat.contact.service.model.repository.ContactRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class ContactSearchCmd implements BusinessLogicCommand {

    @Setter
    private Long accountId;

    @Setter
    private Long userId;

    @Setter
    private Integer limit;

    @Setter
    private Integer page;

    @Setter
    private ContactSearchInput input;

    //Para retornar
    @Getter
    List<Contact> contacts;

    @Getter
    private Integer totalPages;

    @Getter
    private Long totalElements;

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public void execute() {
        contacts = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(page, limit);
        Page<Contact> pageResult = contactRepository.findAll(buildSpecification(), pageRequest);


        List<Contact> content = pageResult.getContent();
        if (!CollectionUtils.isEmpty(content)) {
            contacts.addAll(content);
        }

        totalPages = pageResult.getTotalPages();
        totalElements = pageResult.getTotalElements();

    }

    private Specification<Contact> buildSpecification() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            query.orderBy(cb.desc(root.get(Contact_.createdDate)));
            predicates.add(cb.equal(root.get(Contact_.accountId), accountId));

            if (input.getWithoutAccountOwner() != null && input.getWithoutAccountOwner()) {
                predicates.add(cb.isNull(root.get(Contact_.userId)));
            }

            Join<Contact, Detail> joinDetail = root.join(Contact_.detail);

            if (!StringUtils.isEmpty(input.getInformation())) {
                List<Predicate> detailPredicates = new ArrayList<>();
                detailPredicates.add(cb.like(joinDetail.get(Detail_.information), "%" + input.getInformation() + "%"));

                predicates.add(cb.or(detailPredicates.toArray(new Predicate[0])));
            }

//            addOrderBy(query, joinDetail, cb);

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    //  private void addOrderBy(CriteriaQuery query,
    //                         Join<Contact, Detail> joinDetail,
    //                         CriteriaBuilder cb) {
    //     query.orderBy(cb.asc(joinDetail.get(Detail_.createdDate)));
    //  }
}
