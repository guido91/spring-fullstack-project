package com.dh.chat.contact.service.model.impl;

import com.dh.chat.contact.service.model.domain.Contact;

/**
 * @author Santiago Mamani
 */
public final class ContactBuilder {

    private ContactImpl instance;

    public static ContactBuilder getInstance(Contact contact) {
        return (new ContactBuilder()).setContact(contact);
    }

    private ContactBuilder() {
        instance = new ContactImpl();
    }

    private ContactBuilder setContact(Contact contact) {
        instance.setContactId(contact.getId());
        instance.setUserId(contact.getUserId());
        instance.setAccountId(contact.getAccountId());
        instance.setEmail(contact.getEmail());
        instance.setName(contact.getName());
        instance.setAvatarId(contact.getAvatarId());
        instance.setCreatedDate(contact.getCreatedDate());

        return this;
    }

    public ContactImpl build() {
        return instance;
    }
}
