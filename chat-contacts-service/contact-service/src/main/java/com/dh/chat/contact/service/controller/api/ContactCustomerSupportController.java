package com.dh.chat.contact.service.controller.api;

import com.dh.chat.contact.service.command.CustomerSupportCmd;
import com.dh.chat.contact.service.controller.Constants;
import com.dh.chat.contact.service.model.domain.Contact;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

@Api(
        tags = Constants.ContactTag.NAME,
        description = Constants.ContactTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SECURE_CONTACTS)
@RequestScope
public class ContactCustomerSupportController {

    @Autowired
    private CustomerSupportCmd customerSupportCmd;

    @RequestMapping(method = RequestMethod.GET)
    public List<Contact> getCustomerSupport() {
        customerSupportCmd.execute();
        return customerSupportCmd.getContacts();
    }
}
