package com.dh.chat.contact.service.command;

import com.dh.chat.contact.api.input.ContactCreateInput;
import com.dh.chat.contact.service.model.domain.Contact;
import com.dh.chat.contact.service.model.repository.ContactRepository;
import com.dh.chat.contact.service.model.repository.DetailRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@SynchronousExecution
public class CustomerSupportCmd implements BusinessLogicCommand {

    @Getter
    private List<Contact> contacts;

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public void execute() {
        contacts = contactRepository.getCustomerSupport();
    }
}
