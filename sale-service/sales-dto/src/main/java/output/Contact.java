package output;

import lombok.Data;

import java.util.Date;

@Data
public class Contact {
    private Long contactId;

    private Long userId;

    private Long accountId;

    private String email;

    private String name;

    private String avatarId;

    private Date createdDate;
}
