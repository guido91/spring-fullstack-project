package input;

import lombok.Data;

@Data
public class EmployeeInput {
    private String email;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String position;
}
