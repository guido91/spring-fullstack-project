package input;

import lombok.Data;

import java.util.Date;

@Data
public class ClientInput {
    private String email;
    private String firstName;
    private String lastName;
    private Gender gender;
    private Date lastPurchase;
}
