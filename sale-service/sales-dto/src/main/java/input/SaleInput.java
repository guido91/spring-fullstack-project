package input;

import lombok.Data;

import java.util.Date;

@Data
public class SaleInput {
    private Long clientId;
    private Long employeeId;
    private Long numberSale;
}
