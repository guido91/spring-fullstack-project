package com.dh.fullstack.sale.service.controller;

import com.dh.fullstack.sale.service.command.CreateEmployeeCmd;
import com.dh.fullstack.sale.service.model.domain.Employee;
import input.EmployeeInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    private CreateEmployeeCmd createEmployeeCmd;

    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeInput employeeInput) {
        createEmployeeCmd.setEmployeeInput(employeeInput);
        createEmployeeCmd.execute();
        return createEmployeeCmd.getEmployee();
    }
}
