package com.dh.fullstack.sale.service.model.domain;

public final class Constants {
    private Constants() {
    }

    public static class ClientTable {
        public static final String NAME = "client_table";

        public static class clientId {
            public static final String NAME = "clientid";
        }

        public static class personId {
            public static final String NAME = "personid";
        }

        public static class lastPurchase {
            public static final String NAME = "lastpurchase";
        }
    }

    public static class DetailTable {
        public static final String NAME = "detail_table";

        public static class detailId {
            public static final String NAME = "detailid";
        }

        public static class totalProducts {
            public static final String NAME = "totalproducts";
        }

        public static class totalPrice {
            public static final String NAME = "totalprice";
        }

        public static class saleDetailId {
            public static final String NAME = "saledetailid";
        }
    }

    public static class SaleTable {
        public static final String NAME = "sale_table";

        public static class saleId {
            public static final String NAME = "saleid";
        }

        public static class numberSale {
            public static final String NAME = "numbersale";
        }

        public static class createdDate {
            public static final String NAME = "createddate";
        }

        public static class saleEmployeeId {
            public static final String NAME = "saleemployeeid";
        }

        public static class saleClientId {
            public static final String NAME = "saleclientid";
        }
    }

    public static class EmployeeTable {
        public static final String NAME = "employee_table";

        public static class employeeId {
            public static final String NAME = "employeeid";
        }

        public static class position {
            public static final String NAME = "position";
        }
    }

    public static class PersonTable {
        public static final String NAME = "person_table";

        public static class personId {
            public static final String NAME = "personid";
        }
    }
}
