package com.dh.fullstack.sale.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
class Swagger2Config {

    @Autowired
    private Configurations configurations;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("sales-api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.dh.fullstack.sale.service.controller"))
                                .paths(PathSelectors.any())
                                .build()
                                .apiInfo(apiEndPointsInfo());
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder()
                .title("Sale Service API")
                .description("Sales Management REST API ")
                .contact(new Contact("guido", "", "guidorolando91@gmail.com"))
                .version(configurations.getApiVersion())
                .license("Apache 1.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .build();
    }
}
