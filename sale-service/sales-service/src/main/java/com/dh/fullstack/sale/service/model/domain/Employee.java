package com.dh.fullstack.sale.service.model.domain;

import javax.persistence.*;
import static com.dh.fullstack.sale.service.model.domain.Constants.EmployeeTable;
import static com.dh.fullstack.sale.service.model.domain.Constants.PersonTable;
@Entity
@Table(name = EmployeeTable.NAME)
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = EmployeeTable.employeeId.NAME,
                referencedColumnName = PersonTable.personId.NAME)
})
public class Employee extends Person{

    @Column(name = "position", nullable = false)
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
