package com.dh.fullstack.sale.service.command;

import com.dh.fullstack.sale.service.model.domain.Employee;
import com.dh.fullstack.sale.service.repository.EmployeeRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import input.EmployeeInput;
import org.springframework.beans.factory.annotation.Autowired;

@SynchronousExecution
public class CreateEmployeeCmd implements BusinessLogicCommand {

    @Autowired
    private EmployeeRepository employeeRepository;

    private Employee employee;

    private EmployeeInput employeeInput;

    @Override
    public void execute() {
        employee = employeeRepository.save(composeEmployee(employeeInput));
    }


    private Employee composeEmployee(EmployeeInput employeeInput){
        Employee employee = new Employee();
        employee.setPosition(employeeInput.getPosition());
        employee.setEmail(employeeInput.getEmail());
        employee.setFirstName(employeeInput.getFirstName());
        employee.setLastName(employeeInput.getLastName());
        employee.setGender(employeeInput.getGender());
        return employee;
    }

    public void setEmployeeInput (EmployeeInput employeeInput) {
        this.employeeInput = employeeInput;
    }

    public Employee getEmployee() {
        return this.employee;
    }
}
