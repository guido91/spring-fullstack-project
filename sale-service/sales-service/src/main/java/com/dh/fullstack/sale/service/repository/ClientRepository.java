package com.dh.fullstack.sale.service.repository;

import com.dh.fullstack.sale.service.model.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
