package com.dh.fullstack.sale.service.repository;

import com.dh.fullstack.sale.service.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
