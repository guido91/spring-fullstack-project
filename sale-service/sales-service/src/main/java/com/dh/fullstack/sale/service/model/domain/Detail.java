package com.dh.fullstack.sale.service.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import static com.dh.fullstack.sale.service.model.domain.Constants.DetailTable;
import static com.dh.fullstack.sale.service.model.domain.Constants.SaleTable;

@Entity
@Table(name = DetailTable.NAME)
public class Detail implements Serializable {
    @Id
    @Column(name = DetailTable.detailId.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = DetailTable.totalProducts.NAME, nullable = false)
    private Integer totalProducts;

    @Column(name = DetailTable.totalPrice.NAME, nullable = false)
    private Long totalPrice;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = DetailTable.saleDetailId.NAME, referencedColumnName =
            SaleTable.saleId.NAME, nullable = false)
    private Sale sale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }
}
