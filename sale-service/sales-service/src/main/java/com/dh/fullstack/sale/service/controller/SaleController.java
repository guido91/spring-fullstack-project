package com.dh.fullstack.sale.service.controller;

import com.dh.fullstack.sale.service.command.CreateSaleCmd;
import com.dh.fullstack.sale.service.model.domain.Sale;
import com.dh.fullstack.sale.service.repository.ClientRepository;
import com.dh.fullstack.sale.service.repository.EmployeeRepository;
import com.dh.fullstack.sale.service.repository.SaleGraphRepository;
import com.dh.fullstack.sale.service.repository.SaleRepository;
import input.SaleInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/sale")
public class SaleController {

    @Autowired
    private SaleGraphRepository saleGraphRepository;

    @Autowired
    private CreateSaleCmd createSaleCmd;

    @RequestMapping(method = RequestMethod.POST)
    public Sale createSale(@RequestBody SaleInput saleInput) {
        createSaleCmd.setSaleInput(saleInput);
        createSaleCmd.execute();
        return createSaleCmd.getSale();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Sale> getAllSales(){
        return saleGraphRepository.getAllSale();
    }
}
