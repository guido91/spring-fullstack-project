package com.dh.fullstack.sale.service.command;

import com.dh.fullstack.sale.service.model.domain.Client;
import com.dh.fullstack.sale.service.model.domain.Sale;
import com.dh.fullstack.sale.service.repository.ClientRepository;
import com.dh.fullstack.sale.service.repository.EmployeeRepository;
import com.dh.fullstack.sale.service.repository.SaleRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import input.ClientInput;
import input.SaleInput;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@SynchronousExecution
public class CreateSaleCmd implements BusinessLogicCommand {

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    private Sale sale;

    private SaleInput saleInput;

    @Override
    public void execute() {
        Sale saleInstance = new Sale();
        saleInstance.setClient(clientRepository.findById(saleInput.getClientId()).get());
        saleInstance.setEmployee(employeeRepository.findById(saleInput.getEmployeeId()).get());
        saleInstance.setNumberSale(saleInput.getNumberSale());
        saleInstance.setCreatedDate(new Date());
        sale = saleRepository.save(saleInstance);
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public SaleInput getSaleInput() {
        return saleInput;
    }

    public void setSaleInput(SaleInput saleInput) {
        this.saleInput = saleInput;
    }
}
