package com.dh.fullstack.sale.service.repository;

import com.dh.fullstack.sale.service.model.domain.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaleRepository extends JpaRepository<Sale, Long> {
}
