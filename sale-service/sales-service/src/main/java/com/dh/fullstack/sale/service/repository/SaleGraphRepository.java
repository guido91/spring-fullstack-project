package com.dh.fullstack.sale.service.repository;

import com.dh.fullstack.sale.service.model.domain.Client;
import com.dh.fullstack.sale.service.model.domain.Sale;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleGraphRepository extends JpaRepository<Sale, Long> {

    @EntityGraph(value = "saleGraph", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select sale from Sale sale ")
    List<Sale> getAllSale();
}
