package com.dh.fullstack.sale.service.controller;

import com.dh.fullstack.sale.service.command.CreateClientCmd;
import com.dh.fullstack.sale.service.model.domain.Client;
import input.ClientInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/client")
public class ClientController {

    @Autowired
    private CreateClientCmd createClientCmd;

    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody ClientInput clientInput) {
        createClientCmd.setClientInput(clientInput);
        createClientCmd.execute();
        return createClientCmd.getClient();
    }
}
