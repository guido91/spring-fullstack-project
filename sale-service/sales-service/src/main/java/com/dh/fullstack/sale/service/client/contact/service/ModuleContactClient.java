package com.dh.fullstack.sale.service.client.contact.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import output.Contact;

import java.util.List;

@FeignClient("${contact.service.name}")
public interface ModuleContactClient {
    @RequestMapping(
            value = "/secure/contacts",
            method = RequestMethod.GET
    )
    List<Contact> getCustomerSupport();
}
