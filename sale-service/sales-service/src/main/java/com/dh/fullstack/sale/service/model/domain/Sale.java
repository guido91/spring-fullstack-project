package com.dh.fullstack.sale.service.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import static com.dh.fullstack.sale.service.model.domain.Constants.SaleTable;
import static com.dh.fullstack.sale.service.model.domain.Constants.ClientTable;
import static com.dh.fullstack.sale.service.model.domain.Constants.EmployeeTable;
@Entity
@Table(name = SaleTable.NAME)
@NamedEntityGraph(
        name = "saleGraph",
        attributeNodes = {
                @NamedAttributeNode("employee"),
                @NamedAttributeNode("client")
        }
)
public class Sale implements Serializable {
    @Id
    @Column(name = SaleTable.saleId.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = SaleTable.numberSale.NAME, nullable = false)
    private Long numberSale;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = SaleTable.createdDate.NAME, nullable = false, updatable = false)
    private Date createdDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = SaleTable.saleEmployeeId.NAME, referencedColumnName =
            EmployeeTable.employeeId.NAME, nullable = false)
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = SaleTable.saleClientId.NAME, referencedColumnName =
            ClientTable.clientId.NAME, nullable = false)
    private Client client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
