package com.dh.fullstack.sale.service.controller;

import com.dh.fullstack.sale.service.client.contact.service.ModuleContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import output.Contact;

import java.util.List;

@RestController
@RequestMapping(value = "/customer-support")
public class CustomerSupportController {

    @Autowired
    private ModuleContactService moduleContactService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Contact> getCustomerSupport() {
        return moduleContactService.getCostumerSupport();
    }
}
