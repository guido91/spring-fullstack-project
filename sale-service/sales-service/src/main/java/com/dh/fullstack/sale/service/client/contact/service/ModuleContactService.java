package com.dh.fullstack.sale.service.client.contact.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import output.Contact;

import java.util.List;

@Service
public class ModuleContactService {
    @Autowired
    private ModuleContactClient client;

    public List<Contact> getCostumerSupport() {
        return client.getCustomerSupport();
    }
}
