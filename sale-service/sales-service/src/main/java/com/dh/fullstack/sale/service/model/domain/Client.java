package com.dh.fullstack.sale.service.model.domain;

import javax.persistence.*;
import java.util.Date;

import static com.dh.fullstack.sale.service.model.domain.Constants.ClientTable;

@Entity
@Table(name = ClientTable.NAME)
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = ClientTable.clientId.NAME,
                referencedColumnName = ClientTable.personId.NAME)
})
public class Client extends Person {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = ClientTable.lastPurchase.NAME, nullable = false, updatable = false)
    private Date lastPurchase;


    public Date getLastPurchase() {
        return lastPurchase;
    }

    public void setLastPurchase(Date lastPurchase) {
        this.lastPurchase = lastPurchase;
    }

}
