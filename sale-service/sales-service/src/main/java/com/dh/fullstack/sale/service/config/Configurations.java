package com.dh.fullstack.sale.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:/configuration/api-version.properties")
public class Configurations {
    @Value("${sales.service.version}")
    private String apiVersion;

    public String getApiVersion() {
        return apiVersion;
    }
}
