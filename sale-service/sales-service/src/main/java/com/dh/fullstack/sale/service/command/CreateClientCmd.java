package com.dh.fullstack.sale.service.command;

import com.dh.fullstack.sale.service.model.domain.Client;
import com.dh.fullstack.sale.service.repository.ClientRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import input.ClientInput;
import org.springframework.beans.factory.annotation.Autowired;

@SynchronousExecution
public class CreateClientCmd implements BusinessLogicCommand {

    @Autowired
    private ClientRepository clientRepository;

    private ClientInput clientInput;

    private Client client;

    @Override
    public void execute() {
        client = clientRepository.save(composeClient(clientInput));
    }

    private Client composeClient(ClientInput clientInput) {
        Client client = new Client();
        client.setEmail(clientInput.getEmail());
        client.setLastName(clientInput.getLastName());
        client.setFirstName(clientInput.getFirstName());
        client.setLastPurchase(clientInput.getLastPurchase());
        return client;
    }

    public void setClientInput(ClientInput clientInput) {
        this.clientInput = clientInput;
    }

    public Client getClient() {
        return this.client;
    }
}
